#ifndef MATRIXSHADER_H
#define MATRIXSHADER_H

#include <QOpenGLShaderProgram>
#include <QOpenGLExtraFunctions>
#include <QOpenGLBuffer>
#include <QOpenGLTexture>
class MatrixShader
{
public:
    MatrixShader() = default;
    void initialize();
    void render(QOpenGLExtraFunctions *f,QMatrix4x4 &projM,QMatrix4x4 &came,QMatrix4x4 &mvp);

private:
    QOpenGLShaderProgram m_program;
    QOpenGLBuffer m_vbo;
    QOpenGLTexture *m_texs[6];
};

#endif // MATRIXSHADER_H
