#include "matrixshader.h"

void MatrixShader::initialize()
{
    m_program.addCacheableShaderFromSourceFile(QOpenGLShader::Vertex,"vsrc.vsh");
    m_program.addCacheableShaderFromSourceFile(QOpenGLShader::Fragment,"fsrc.fsh");
    m_program.link();

    const GLfloat coords[6][8][3] = {
        { { +1, -1, -1 }, { -1, -1, -1 }, { -1, +1, -1 }, { +1, +1, -1 } },
        { { +1, +1, -1 }, { -1, +1, -1 }, { -1, +1, +1 }, { +1, +1, +1 } },
        { { +1, -1, +1 }, { +1, -1, -1 }, { +1, +1, -1 }, { +1, +1, +1 } },
        { { -1, -1, -1 }, { -1, -1, +1 }, { -1, +1, +1 }, { -1, +1, -1 } },
        { { +1, -1, +1 }, { -1, -1, +1 }, { -1, -1, -1 }, { +1, -1, -1 } },
        { { -1, -1, +1 }, { +1, -1, +1 }, { +1, +1, +1 }, { -1, +1, +1 } }
    };
    for(int i = 0; i < 6; i++){
        m_texs[i] = new QOpenGLTexture(QImage(QString("images/side%1.png").arg(i + 1)).mirrored());
    }
    QVector<GLfloat> vertData;
    for (int i = 0; i < 6; ++i) {
        for (int j = 0; j < 4; ++j) {
            // vertex position
            vertData.append(coords[i][j][0]);
            vertData.append(coords[i][j][1]);
            vertData.append(coords[i][j][2]);
            // texture coordinate
            vertData.append(j == 0 || j == 3);
            vertData.append(j == 0 || j == 1);
        }
    }
    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vertData.constData(),vertData.count() * sizeof(GLfloat));
}

void MatrixShader::render(QOpenGLExtraFunctions *f, QMatrix4x4 &projM, QMatrix4x4 &came, QMatrix4x4 &mvp)
{
    f->glEnable(GL_DEPTH_TEST);
    f->glEnable(GL_CULL_FACE);

    m_program.bind(); //使用m_program的一套渲染器
    m_vbo.bind(); //将开避在opengl server端的内存绑定过来 以便于渲染使用
    f->glActiveTexture(GL_TEXTURE0 + 2); //激活2号纹理，用于渲染使用
    m_program.setUniformValue("uProjMatrix",projM);
    m_program.setUniformValue("camMatrix",came);
    m_program.setUniformValue("uMVPMatrix",mvp);
    m_program.setUniformValue("uTexture",2); //指明栅格化着色器中的纹理单元为激活的2号
    m_program.enableAttributeArray(0); //使能顶点属性
    m_program.enableAttributeArray(1); //使能纹理属性
    m_program.setAttributeBuffer(0,GL_FLOAT,0,3,5*sizeof(GLfloat)); //设置顶点数据
    m_program.setAttributeBuffer(1,GL_FLOAT,3 * sizeof(GLfloat),2,5 * sizeof(GLfloat)); //设置纹理数据

    for(int i = 0; i < 6; i++){
        m_texs[i]->bind(2); //将纹理数据绑定到激活的2号纹理单元，将纹理数据传递到了激活的实际的纹理单元上
        f->glDrawArrays(GL_TRIANGLE_FAN, i * 4, 4);
    }

    m_program.disableAttributeArray(0);
    m_program.disableAttributeArray(1);
    m_vbo.release();
    m_program.release();

    f->glDisable(GL_DEPTH_TEST);
    f->glDisable(GL_CULL_FACE);
}
