#include "widget.h"
#include <QWheelEvent>
#include <QDebug>

Widget::Widget(QWidget *parent)
    : QOpenGLWidget(parent),
      m_target(0,0,-1)
{
    m_tm.setInterval(60);
    connect(&m_tm,SIGNAL(timeout()),this,SLOT(slotTimeout()));
    m_tm.start();
}

Widget::~Widget()
{

}

void Widget::resizeGL(int w, int h)
{
    m_proMM.setToIdentity();
    m_proMM.perspective(60.0f,GLfloat(w)/h,0.01f,100.0f);
}

void Widget::initializeGL()
{
    m_matxShader.initialize();
}

void Widget::paintGL()
{
    QOpenGLExtraFunctions *f = QOpenGLContext::currentContext()->extraFunctions();
    f->glClearColor(0.0, 0.0, 0.0, 0.0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    QMatrix4x4 mvpM;
    mvpM.rotate(30,1,0,0);
    mvpM.rotate(m_angle,0,1,0);
    mvpM.translate(4,0,0);
    mvpM.rotate(m_curX,1,0,0);
    mvpM.rotate(m_curY,0,1,0);
    mvpM.rotate(m_curZ,0,0,1);

    QMatrix4x4 camera;
    m_eye.setZ(m_ez);
    camera.lookAt(m_eye,m_eye + m_target,QVector3D(0,1,0));
    m_matxShader.render(f,m_proMM,camera,mvpM);
}

void Widget::slotTimeout()
{
    m_angle += 5;
    m_curX += 5;
    m_curY += 5;
    m_curZ += 5;
    update();
}

void Widget::wheelEvent(QWheelEvent *event)
{
    if (! event->pixelDelta().isNull()) {
        m_ez += event->pixelDelta().y();
    } else if (!event->angleDelta().isNull()) {
        m_ez += (event->angleDelta() / 120).y();
    }

    event->accept();
    update();
}
