#version 330
uniform mat4 uMVPMatrix;
uniform mat4 uProjMatrix;
uniform mat4 camMatrix;
layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec2 aTexture;
smooth out vec4 vColor;

void main(void)
{
    gl_Position = uProjMatrix * camMatrix * uMVPMatrix * vec4(aPosition,1); //注意顺序，先乘的后作用
    vColor = vec4(aTexture,1,1);
}
