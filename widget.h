#ifndef WIDGET_H
#define WIDGET_H

#include <QOpenGLWidget>
#include "matrixshader.h"
#include <QTimer>
class Widget : public QOpenGLWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = 0);
    ~Widget();

protected:
    void resizeGL(int w, int h) override;
    void initializeGL() override;
    void paintGL() override;
    void wheelEvent(QWheelEvent *event) override;

private:
    MatrixShader m_matxShader;
    QMatrix4x4 m_proMM;
    QVector3D m_eye,m_target;
    float m_angle = 0;
    float m_curX = 0,m_curY = 0,m_curZ = 0;
    float m_ez = 5;
    QTimer m_tm;

private slots:
    void slotTimeout();
};

#endif // WIDGET_H
