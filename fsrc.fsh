#version 330
uniform sampler2D uTexture;
smooth in vec4 vColor;
out vec4 fragColor;

void main(void)
{
    fragColor = texture2D(uTexture,vColor.rg);
}
